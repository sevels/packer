packer {
  required_plugins {
    virtualbox = {
      version = "~> 1"
      source = "github.com/hashicorp/virtualbox"
    }
    vagrant = {
      version = "~> 1"
      source = "github.com/hashicorp/vagrant"
  }
}
}
#testcomment
/*locals {

  ## Autoinstall targets
    ## ai = autoinstall
  ai_target_dir      = "${var.vm_name}/"
  ai_data_source     = "ds=\"nocloud-net;seedfrom=http://{{ .HTTPIP }}:{{ .HTTPPort }}/${local.ai_target_dir}\""
  ai_boot_ip_addr    = "ip=${var.vm_nic1_ip_address}::${var.vm_nic1_ip_gateway}:${var.vm_nic1_ip_netmask}:${var.vm_name}:::${var.vm_nic1_nameservers[0]}:${var.vm_nic1_nameservers[1]}"
}
*/
# Define a source
source "virtualbox-iso" "almalinux" {
  guest_os_type = "RedHat9_64"
  iso_checksum = "4a8c4ed4b79edd0977d7f88be7c07e12c4b748671a7786eb636c6700e58068d5"
  iso_url = "https://repo.almalinux.org/almalinux/9.3/isos/x86_64/AlmaLinux-9.3-x86_64-dvd.iso"
  ssh_username = "vagrant"
  ssh_password = "vagrant"
  communicator = "ssh"
  ssh_handshake_attempts = 200
  http_directory = "http"
  vboxmanage = [["modifyvm", "{{ .Name }}", "--memory", "8192"], ["modifyvm", "{{ .Name }}", "--cpus", "8"], ["modifyvm", "{{.Name}}", "--nat-localhostreachable1", "on"]]
  boot_wait = "5s"
  boot_command = [
            "<wait><up><wait><tab> inst.text inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/userdata.cfg<enter><wait>"
    ]
  shutdown_command = "echo 'vagrant' | sudo -S shutdown -P now"
}

# Define a build
build {
  sources = ["source.virtualbox-iso.almalinux"]

  provisioner "shell" {
  
    scripts = [
      "./create_vagrant_user.sh",
      #"./update.sh",
      "./guestadditions.sh",
      #"./setup.sh"
    ]
  }
  
  
  
  post-processor "vagrant" {
    compression_level = 9   
  }
}



/* "apt-get update",
      "apt-get upgrade -y",
      "apt-get install -y apache2" */
/* "c<wait>",
    "linux /casper/vmlinuz autoinstall<enter>",
    "initrd /casper/initrd<enter>",
    "boot<enter>" */

/* "<wait>e<wait><down><wait><down><wait><down><wait><end>",
    " autoinstall ds=nocloud-net;s=http://{{.HTTPIP}}:{{.HTTPPort}}/;",
    "<wait>",
    "<F10>"*/

    /*<enter><wait><enter><wait><f6><wait><esc><wait> ",
    "autoinstall ds=nocloud-net;s=http://{{.HTTPIP}}:{{.HTTPPort}}/",
    "<enter>"*/