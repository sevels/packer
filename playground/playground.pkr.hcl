packer {
  required_plugins {
    virtualbox = {
      version = "~> 1"
      source = "github.com/hashicorp/virtualbox"
    }
    vagrant = {
      version = "~> 1"
      source = "github.com/hashicorp/vagrant"
  }
}
}

  variable "source_name" {
    description = "Name of the source"
    type = string
    default = "Ubuntu-Server"
  }


# Source Ubuntu-Server
source "virtualbox-iso" "Ubuntu-Server" {
  guest_os_type = "Ubuntu_64"
  iso_checksum = "a4acfda10b18da50e2ec50ccaf860d7f20b389df8765611142305c0e911d16fd"
  iso_url = "http://releases.ubuntu.com/jammy/ubuntu-22.04.3-live-server-amd64.iso"
  ssh_username = "vagrant"
  ssh_password = "vagrant"
  communicator = "ssh"
  ssh_handshake_attempts = 200
  http_directory = "http"
  vboxmanage = [["modifyvm", "{{ .Name }}", "--memory", "8192"], ["modifyvm", "{{ .Name }}", "--cpus", "8"], ["modifyvm", "{{.Name}}", "--nat-localhostreachable1", "on"]]
  boot_wait = "5s"
  boot_command = [
       "e<wait>",
        "<down><down><down><end>",
        " autoinstall ds=nocloud-net\\;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}/", # Mit diesem Command greift Packer auf den Web Server zu, bei dem die Dateien für Cloud-Init gespeichert sind.
        "<f10>"
    ]
  shutdown_command = "echo 'vagrant' | sudo -S shutdown -P now"
}

#Source AlmaLinux
source "virtualbox-iso" "almalinux" {
  guest_os_type = "RedHat9_64"
  iso_checksum = "4a8c4ed4b79edd0977d7f88be7c07e12c4b748671a7786eb636c6700e58068d5"
  iso_url = "https://repo.almalinux.org/almalinux/9.3/isos/x86_64/AlmaLinux-9.3-x86_64-dvd.iso"
  ssh_username = "vagrant"
  ssh_password = "vagrant"
  communicator = "ssh"
  ssh_handshake_attempts = 200
  http_directory = "http"
  vboxmanage = [["modifyvm", "{{ .Name }}", "--memory", "8192"], ["modifyvm", "{{ .Name }}", "--cpus", "8"], ["modifyvm", "{{.Name}}", "--nat-localhostreachable1", "on"]]
  boot_wait = "5s"
  boot_command = [
            "<wait><up><wait><tab> inst.text inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/userdata.cfg<enter><wait>"
    ]
  shutdown_command = "echo 'vagrant' | sudo -S shutdown -P now"
}

# Define a build
build {
  sources = ["source.virtualbox-iso.${var.source_name}"]
  provisioner "shell" {
    scripts = [
      "create_vagrant_user.sh",
      "update.sh",
      "guestadditions.sh",
      "setup.sh"
    ]
    only = ["virtualbox-iso.Ubuntu-Server"]
  }
  provisioner "shell" {
    scripts = [
      "./create_vagrant_user_alma.sh",
      "./guestadditions_alma.sh",
    ]
    only = ["virtualbox-iso.almalinux"]
  }


  post-processor "vagrant" {
    compression_level = 9 
    output = "${formatdate("YYYY.MM.DD", "${timestamp()}")}_${var.source_name}_${formatdate("hh:mm:ss_ZZZ+1", "${timeadd(timestamp(), "1h")}")}"
  
  }

}

